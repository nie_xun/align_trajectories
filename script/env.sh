#!/bin/bash

align_tab="/etc/bash_completion.d/align_trajectories" 

if [ ! -f ${align_tab} ]; then
  touch ${align_tab}

cat>${align_tab}<<EOF
#!/usr/bin/env bash

_align_traj_tab()
{
    local cur prev opts
    COMPREPLY=()
    cur="\${COMP_WORDS[COMP_CWORD]}"
    prev="\${COMP_WORDS[COMP_CWORD-1]}"
    opts="--help --align --scale-align --euler-reverse --display --output --ref --t-offset --use-umeyama --use-frank --use-salas --plot-matchline --consider-rotations -a -h -s -r -p -o -t"

    if [[ \${cur} == -* ]] ; then
        COMPREPLY=( \$(compgen -W "\${opts}" -- \${cur}) )
        return 0
    fi  
}
complete -o default -F _align_traj_tab align_trajectories
EOF

export PATH=$PATH:/home/niebaozhen/workspace/tmp/src/align_trajectories/build/
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/niebaozhen/workspace/tmp/src/align_trajectories/build/

fi 
source ~/.bashrc
