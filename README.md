## USAGE

### Download & make��
```bash
git clone https://gitee.com/nie_xun/align_trajectories.git
cd <align_trajectories_dir>
mkdir build
cd build
cmake ..
make & sudo make env
```
> `make env` make the tab completion effective

### Run
```bash
./align_trajectories -h
Usage: 
	 align_trajectories [opts] --ref [groundtruth].txt [estimate].txt
	 -a --align	 Align trajectories by calculating the transformation
	 -h --help	 Print the instructions of this tool
	 -s --scale-align	 Evaluate scale when aligning trajectories. This need -a.
	 -t --t-offset [timeoffset es-gt]	 Set the time offset. If the t-offset isn't set, the t-offset will be calculated automatically
	 -r --euler-reverse	 This option is deprecated !!! Make Euler sign opposite, due to different pose definition.The sign vector for trajs U need to input, which default to be 1.
	 -p --display	 Use pangolin to display trajectories
	 -o --output	 Output results of aligning

	 --ref	 Indicates the file which saves groundtruth
	 --use-umeyama	 Utilizes umeyama method for aligning trajectories
	 --plot-matchline	 Display matchline in pangolin
```

run exapmles
```bash
./align_trajectories --ref  ../data/0507/Groundtruth.txt ../data/0507/Estimate.txt -asp -o ../output/ --use-umeyama
./align_trajectories ../data/multi/ademo.txt -o ../output
./align_trajectories --ref  ../data/2/stamped_groundtruth.txt ../data/2/stamped_traj_estimate.txt -o ../output
./align_trajectories --ref  ../data/multi/ademo.txt ../data/multi/orb.txt  ../data/multi/demo.txt -o ../output/ --plot-matchline
```

### results
The evaluation results are save in data/data_multi_xxx_evaluation_results.txt
The data txt that after processing is saved in dir output/data
The scripts for plot is generated in output

### plot use gnuplot
After run align_trajectories, there are data files will be out in <output_dir> that you specified, concludes the trajectories and euler angles txt file, besides, the gnuplot scripts which can be used to plot data.
you also can plot these data by using matlab
```bash
cd <output>
bash <*>_plot.sh

#examples
bash data_multi_orb_gnuplot.sh
```
![gnuplot_example](https://gitee.com/nie_xun/align_trajectories/raw/master/output/gnuplot_example.png)

### plot use matlab
The scripts of matlab will be output under the folder of <output>/matlab_scirpts when the option [-o/--output] was be set, which are list as follows:
>all_es2gt_matlab.m
xxx_es2gt_matlab.m
xxx_es2gt_matlab.m
xxx_es_matlab.m

Usage:
1. You should copy the data and matlab_scripts folders into windows which installed matlab.
You can use scp tools also to copy files.  

data: <output>/data
matlab scripts: <output>/matlab_scirpts

The dir structrue should like this below:
>
|-- date(just support the 'data' name)  
|&emsp;&emsp;   ||-- data_kitti_00_00_tum_trajectory.txt  
|&emsp;&emsp;   ||-- data_kitti_00_aloam_traj_euler.txt  
|&emsp;&emsp;   ||-- data_kitti_00_aloam_traj_evaluation_results.txt  
|&emsp;&emsp;   ||-- data_kitti_00_aloam_traj_trajectories.txt  
|&emsp;&emsp;   ||-- data_kitti_00_aloam_traj_trajectory.txt  
|&emsp;&emsp;   ||-- data_kitti_00_aloam_traj_xyz_drift.txt  
|-- matlab_scripts(named whatever)  
&emsp;&emsp;&emsp;   ||-- all_es2gt_matlab.m  
&emsp;&emsp;&emsp;   ||-- data_kitti_00_00_tum_es_matlab.m  
&emsp;&emsp;&emsp;   ||-- data_kitti_00_aloam_traj_es2gt_matlab.m  
&emsp;&emsp;&emsp;   ||-- data_kitti_00_aloam_traj_es_matlab.m  

2. Operate in matlab
1) Change directory to <matlab_scirpts> folder
eg: type in matlab command bar
```bash
cd E:\align_traj\output\matlab_scirpts\
```
2) run scripts:

eg: type in matlab command bar
```bash
all_es2gt_matlab
data_multi_demo_es2gt_matlab
data_multi_orb_es2gt_matlab
data__trajectory_es_matlab
```
Or you can open these scripts and use the run button to plot curves
![matlab_example4](https://gitee.com/nie_xun/align_trajectories/raw/master/output/matlab_scripts/examples/example4.png)
![matlab_example5](https://gitee.com/nie_xun/align_trajectories/raw/master/output/matlab_scripts/examples/example5.png)
![matlab_example1](https://gitee.com/nie_xun/align_trajectories/raw/master/output/matlab_scripts/examples/matlab_examples1.png)
![matlab_example2](https://gitee.com/nie_xun/align_trajectories/raw/master/output/matlab_scripts/examples/matlab_examples2.png)
![matlab_example3](https://gitee.com/nie_xun/align_trajectories/raw/master/output/matlab_scripts/examples/matlab_examples3.png)
![matlab_example3](https://gitee.com/nie_xun/align_trajectories/raw/master/output/matlab_scripts/examples/fx.png)
![matlab_example3](https://gitee.com/nie_xun/align_trajectories/raw/master/output/matlab_scripts/examples/kitti.png)
